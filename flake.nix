{
  description = "Thunk!";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  };

  outputs = {
    self,
    nixpkgs,
  }: let
    system = "x86_64-linux";
    pkgs = import nixpkgs {
      inherit system;
      config.allowUnfreePredicate = pkg: true;
    };
  in {
    devShells.x86_64-linux = {
      default = pkgs.mkShell {
        buildInputs = with pkgs; [
          (SDL2.override {waylandSupport = true;})
          SDL2_ttf
          glfw
          (glew.override {enableEGL = true;})
          glm
        ];
        shellHook = ''
          export SDL_VIDEODRIVER="wayland"
        '';
      };
    };
  };
}
