#include <SDL2/SDL.h>
#include <SDL2/SDL_syswm.h>


SDL_Window* sdl_begin(char* name, int width, int height, uint32_t windowFlags);
SDL_SYSWM_TYPE sdl_test_wayland(SDL_Window* win);
void sdl_end(SDL_Window* win);
