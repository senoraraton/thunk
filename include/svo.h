#ifndef SVO_H
#define SVO_H

#include <stdbool.h>
#include "voxel.h"

#define MAX_CHILDREN 8
#define MAX_DEPTH 3

struct SVO_Node;

typedef struct BoundingBox {
    float center[3];
    float half_size;
} BoundingBox;

typedef struct SVO_LeafNode {
    Voxel voxel;
} SVO_LeafNode;

typedef struct SVO_InternalNode {
    struct SVO_Node* children[MAX_CHILDREN];
} SVO_InternalNode;

typedef struct SVO_Node {
    bool isLeaf;
    int depth;
    BoundingBox bounding_box;
    union {
        SVO_LeafNode* leaf_node;
        SVO_InternalNode* internal_node;
    };
} SVO_Node;

SVO_Node* svo_init_tree(uint32_t width);
unsigned int svo_count_nodes(const SVO_Node* root);
void svo_print_nodes(SVO_Node* root);
void svo_destroy_tree(SVO_Node* node);
SVO_Node** svo_get_voxels(SVO_Node* root, int* voxelCount);

#endif
