#ifndef DCEL_H
#define DCEL_H

#include<stdio.h>

struct Site;

typedef struct Vertex {
    double x, y;
} Vertex;

typedef struct HalfEdge {
    Vertex* origin;
    Vertex* dest;
    struct HalfEdge* twin;
    struct HalfEdge* next;
    struct HalfEdge* prev;
    struct Face* incidentFace;
} HalfEdge;

typedef struct Face {
    struct Site* site;
    HalfEdge* outerComponent;
} Face;

typedef struct Site {
    size_t index;
    double x,y;
    Face* face;
} Site;

typedef struct DCEL {
    Vertex* vertices;
    HalfEdge* halfEdges;
    Face* faces;
    int numVertices;
    int numHalfEdges;
    int numFaces;
    int vertexCapacity;
    int halfEdgeCapacity;
    int faceCapacity;
} DCEL;

void traverse_Face(Face* face);

#endif
