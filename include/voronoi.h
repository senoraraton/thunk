#ifndef VORONOI_H
#define VORONOI_H

#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "dcel.h"
#include "pqueue.h"

typedef struct {
    int x;
    int y;
    int region;
} GridPoint;

typedef struct {
    int x;
    int y;
} SeedPoint;

typedef enum { RED, BLACK } Color;

typedef struct Arc {
    struct Arc* parent;
    struct Arc* left;
    struct Arc* right;
    Site* site;
    HalfEdge* leftHalfEdge;
    HalfEdge* rightHalfEdge;
    Event* event;
    struct Arc* prev;
    struct Arc* next;
    Color color;
} Arc;


void generate_seed_points(SeedPoint* points, int num_points, int width, int height);
void compute_voronoi(GridPoint* grid, SeedPoint* points, int num_points, int width, int height);
void handleSiteEvent(PriorityQueue* pq, DCEL* dcel, Site* site);
void handleCircleEvent(PriorityQueue* pq, DCEL* dcel, Event event);
void initalizeEventQueue(PriorityQueue* pq, Site* site, int numSites);
void processEvents(PriorityQueue* pq, DCEL* dcel);

#endif
