#ifndef GL_HELPER_H
#define GL_HELPER_H

#include <GL/glew.h>
#include "svo.h"

GLuint compileShaderFromFile(GLenum shaderType, const char* filepath);
GLuint compileShaderFromSource(GLenum shaderType, const char* source);
GLuint linkProgramFromShaders(GLuint vertexShader, GLuint fragmentShader);
GLfloat* glPackVoxels(SVO_Node** packed_voxels, int* voxelCount, bool normalize);
void normalizeVertices(SVO_Node** data, int *count);
void glShaderCheckError(GLuint shader);
void glProgramCheckError(GLuint program);

#endif
