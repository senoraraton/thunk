#ifndef VOXEL_H
#define VOXEL_H
#include <stdint.h>

#define MAX_NAME_LENGTH 64
#define MAX_SPRITE_LENGTH 256

typedef struct {
    uint8_t rgba[4];
    bool destructible;
    uint8_t element;
    char name[MAX_NAME_LENGTH];
    char sprite[MAX_SPRITE_LENGTH];
} Voxel;

#endif
