#ifndef PERLIN_H
#define PERLIN_H

void init_perlin();
double noise (double x, double y, double z);

#endif
