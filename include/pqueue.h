#ifndef PQUEUE_H
#define PQUEUE_H

typedef struct {
    double x,y;
    int type;
} Event;

typedef struct {
    Event* array;
    int capacity;
    int size;
} PriorityQueue;

PriorityQueue* createPriorityQueue(int capacity);
void destroyPriorityQueue(PriorityQueue* pq);
void insertEvent(PriorityQueue* pq, Event event);
Event extractMin(PriorityQueue* pq);
Event* findEvent(PriorityQueue* pq, Event event);
void removeEvent(PriorityQueue* pq, Event event);


#endif
