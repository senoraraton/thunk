#include <unistd.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stdio.h>

int test_gl_init() {
    GLFWwindow* window;

    if (!glfwInit()) {
        fprintf(stderr, "Failed to init GLFW\n");
        return -1;
    } else {
        fprintf(stderr, "Glfw Init success.\n");
    }

    window = glfwCreateWindow(640, 480, "OpenGL Test", NULL, NULL);
    if (!window) {
        fprintf(stderr, "Failed to create GLFW window\n");
        glfwTerminate();
        return -1;
    } else {
        printf("Window created: %p\n", (void*)window);
    }

    glfwMakeContextCurrent(window);

    GLFWwindow* test = glfwGetCurrentContext();
    printf("GLFW context: %p\n", (void*)test);

    GLenum err = glewInit();
    if (err != GLEW_OK) {
        fprintf(stderr, "Failed to  init GLEW: %s\n", glewGetErrorString(err));
        glfwTerminate();
        return -1;
    } else {
        fprintf(stderr, "Init GLEW success.\n");
    }

    const GLubyte* renderer = glGetString(GL_RENDERER);
    const GLubyte* version = glGetString(GL_VERSION);
    printf("OpenGL Renderer: %s\n", renderer);
    printf("OpenGL Version: %s\n", version);
    printf("GLFW Version: %s\n", glfwGetVersionString());
    printf("GLEW Version: %s\n", glewGetString(GLEW_VERSION));

    glfwTerminate();
    return 0;
}

int test_gl_render() {
    
    return 0;
}
