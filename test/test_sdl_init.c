#include <SDL2/SDL.h>
#include "test_functions.h"
#include "sdl.h"

int test_sdl_init() {
    SDL_version compiled;
    SDL_version linked;

    SDL_VERSION(&compiled);
    SDL_GetVersion(&linked);

    printf("SDL2 Version: %d.%d.%d (compiled) / %d.%d.%d (linked)\n",
           compiled.major, compiled.minor, compiled.patch,
           linked.major, linked.minor, linked.patch);

    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        fprintf(stderr, "SDL Video init failed.\n");
        return 1; 
    }
    SDL_Quit();
    return 0; 
}
