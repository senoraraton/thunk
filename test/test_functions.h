#ifndef TEST_FUNCTIONS_H
#define TEST_FUNCTIONS_H

typedef struct {
    const char *name;
    const char *category;
    int (*test_func)();
} TestFunction;

#define TEST_FUNCTIONS \
    X("Test SDL init", "SDL", test_sdl_init) \
    X("Test OpenGL/Glew init", "GL", test_gl_init) \
    X("Vornoi Seed Point SDL Render", "SDL", test_voronoi) \
    X("OpenGL Render test", "GL", test_gl_render) \

#define X(name, category, func) int func();
    TEST_FUNCTIONS
#undef X

#endif // TEST_FUNCTIONS_H
