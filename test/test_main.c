#include <stdio.h>
#include "test_functions.h"

#define X(name, category, func) { name, category, func},
TestFunction test_functions[] = {
    TEST_FUNCTIONS
};
#undef X

int main() {
    size_t num_tests = sizeof(test_functions) / sizeof(test_functions[0]);
    printf("Number of tests registered: %zu\n\n", num_tests);

    for (size_t i = 0; i < num_tests; ++i) {
        printf("Running: (%s) %s\n", test_functions[i].category, test_functions[i].name);
        int result = test_functions[i].test_func();
        if (result == 0) {
            printf("\033[32mSuccess.\033[0m\n\n"); // Green for success
        } else {
            printf("\033[31mFail.\033[0m\n\n"); // Red for fail
        }
    }
    return 0;
}
