#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define WIDTH 800
#define HEIGHT 600
#define NUM_POINTS 50

typedef struct {
    int x;
    int y;
} Point;

SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;
SDL_Event event;

Point points[NUM_POINTS];

void init_sdl() {
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        fprintf(stderr, "SDL could not init.  SDL_ERROR: %s\n", SDL_GetError());
        exit(1);
    }
    window = SDL_CreateWindow("Voronoi Diagram Test", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
           WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
    if (window == NULL) {
        fprintf(stderr, "Window could not be created.  SDL_Error: %s\n", SDL_GetError());
        exit(1);
    }
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == NULL) {
        fprintf(stderr, "Renderer could not be created. SDL_Error: %s\n", SDL_GetError());
        exit(1);
    }
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
    SDL_RenderClear(renderer);
}

void render_seed_points() {
    SDL_RenderClear(renderer);
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    for (int i = 0; i < NUM_POINTS; ++i) {
        SDL_RenderDrawPoint(renderer, points[i].x, points[i].y);
    }
    SDL_RenderPresent(renderer);
}

void close_sdl() {
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void generate_random_points() {
    srand(time(NULL));
    for (int i = 0; i < NUM_POINTS; ++i) {
        points[i].x = rand() % WIDTH;
        points[i].y = rand() % HEIGHT;
    }
}

void draw_line(Point p1, Point p2) {
    SDL_RenderDrawLine(renderer, p1.x, p1.y, p2.x, p2.y);
}

void render_voronoi() {
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    for (int i = 0; i < NUM_POINTS; ++i) {
        for (int j = i + 1; j < NUM_POINTS; ++j) {
            Point mid;
            mid.x = (points[i].x + points[j].x) / 2;
            mid.y = (points[i].y + points[j].y) / 2;

            int dx = points[j].x - points[i].x;
            int dy = points[j].y - points[i].y;

            Point p1, p2;
            if (dx == 0) {
                p1.x = p2.x = mid.x;
                p1.y = 0;
                p2.y = HEIGHT;
            } else if (dy == 0) {
                p1.y = p2.y = mid.y;
                p1.x = 0;
                p2.x = WIDTH;
            } else {
                double slope = -(double)dx / dy;
                double intercept = mid.y - slope * mid.x;

                p1.x = 0;
                p1.y = intercept;
                p2.x = WIDTH;
                p2.y = slope * WIDTH + intercept;
            }
            
            draw_line(p1, p2);
        }
    }
    SDL_RenderPresent(renderer);
}

int test_voronoi() {
    init_sdl();

    generate_random_points();
    render_seed_points();
    render_voronoi();

    while (1) {
        if (SDL_PollEvent(&event) && event.type == SDL_QUIT) {
            break;
        }
    }
    close_sdl();

    return 0;
}
