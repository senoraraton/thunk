#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "svo.h"

#define TRACE_ENTER printf("Entering %s\n", __func__)
#define TRACE_EXIT printf("Exiting %s\n", __func__)

static void initalize_children(BoundingBox parentBB, SVO_InternalNode* node, uint32_t depth);
static void init_bounding_box(BoundingBox parentBB, SVO_Node* child, unsigned int idx);
static void svo_create_tree(SVO_Node* parent);
static void svo_count_voxels(SVO_Node* node, SVO_Node** voxels, int* voxelCount);

SVO_Node* svo_init_tree(uint32_t width) {
        SVO_Node* root = (SVO_Node*)malloc(sizeof(SVO_Node));
        if(root == NULL) {
            perror("Unable to malloc root SVO_Node*.\n");
            return NULL;
        }

        root->depth = 0;
        for(int i = 0; i < 3; ++i) {
            root->bounding_box.center[i] = (float) width / 2.0f;
            root->bounding_box.half_size = (float) width / 2.0f;
        }

        if(MAX_DEPTH > 1) {
            root->isLeaf = false;
            root->internal_node = (SVO_InternalNode*)malloc(sizeof(SVO_InternalNode));
            if (root->internal_node == NULL) {
                perror("Failed to allocate memory for SVO_Internalroot");
                free(root);
                return NULL;
            }
            svo_create_tree(root);
        } else {
            root->isLeaf = true;
            root->leaf_node = (SVO_LeafNode*)malloc(sizeof(SVO_LeafNode));
            if (root->leaf_node == NULL) {
                perror("Failed to allocate memory for SVO_LeafRoot");
                free(root);
                return NULL;
            }
        }
    return root;
}

static void svo_create_tree(SVO_Node* parent) {
    if (parent == NULL) {
        perror("Parent in create tree is NULL");
    }

    if (parent->internal_node != NULL && (parent->depth < MAX_DEPTH)) {
        initalize_children(parent->bounding_box, parent->internal_node, parent->depth);
        for(int i = 0; i < MAX_CHILDREN; ++i) {
            svo_create_tree(parent->internal_node->children[i]);
        }
    } else if (parent->leaf_node != NULL) {
        parent->leaf_node->voxel.rgba[0] = 255;
        parent->leaf_node->voxel.rgba[1] = 128;
        parent->leaf_node->voxel.rgba[2] = 0;
        parent->leaf_node->voxel.rgba[3] = 255;
        parent->leaf_node->voxel.destructible = false;
        parent->leaf_node->voxel.element = 1;
        strcpy(parent->leaf_node->voxel.name, "Test_Name");
        strcpy(parent->leaf_node->voxel.sprite, "Test_Sprite");
    } 
}
        
unsigned int svo_count_nodes(const SVO_Node* node) {
    if (node == NULL) {
        return 0;
    }
    if (node->isLeaf) {
        return 1;
    } else {
        unsigned int count = 1;
        for (int i = 0; i < MAX_CHILDREN; ++i) {
            count += svo_count_nodes(node->internal_node->children[i]);
        }
        return count;
    }
}

void svo_print_nodes(SVO_Node* node) {
    if(node == NULL){
        printf("Node is NULL\n");
        return;
    }
    printf("%s Node at depth %d with bounding box center (%f, %f, %f) and half size (%f)\n",
        node->isLeaf ? "Leaf" : (node->internal_node ? "Internal" : "Unknown"),
        node->depth,
        node->bounding_box.center[0],
        node->bounding_box.center[1],
        node->bounding_box.center[2],
        node->bounding_box.half_size); 
    if(!node->isLeaf) {
        for (int i = 0; i < MAX_CHILDREN; ++i) {
            svo_print_nodes(node->internal_node->children[i]);
        }
    } 
}

void svo_destroy_tree(SVO_Node* node) {
    if (node == NULL) {
        return;
    }

    if (node->leaf_node != NULL) {
        free(node->leaf_node);
    } else if (node->internal_node != NULL) {
        for (int i = 0; i < MAX_CHILDREN; ++i) {
            svo_destroy_tree(node->internal_node->children[i]);
        }
    }
    free(node);
}

static void init_bounding_box(BoundingBox parentBB, SVO_Node* child, unsigned int idx) {
    child->bounding_box.half_size = parentBB.half_size / 2.0f;
    for (int i = 0; i < 3; ++i){
        float offset = (idx & (1 << i)) ? child->bounding_box.half_size : -child->bounding_box.half_size;
        child->bounding_box.center[i] = parentBB.center[i] + offset;
    }
}

static void initalize_children(BoundingBox parentBB, SVO_InternalNode* node, uint32_t depth) {
    if (depth > MAX_DEPTH - 1) {
        perror("Internal node parent depth is > (Max Depth - 1).");
    }
    if(depth == (MAX_DEPTH - 1)) {
        for (int i = 0; i < MAX_CHILDREN; ++i) {
            node->children[i] = (SVO_Node*)malloc(sizeof(SVO_Node));
            if (node->children[i] == NULL) {
                perror("Failed to allocate memory for SVO_node child");
            }
            node->children[i]->leaf_node = (SVO_LeafNode*)malloc(sizeof(SVO_LeafNode));
            if (node->children[i]->leaf_node == NULL) {
                perror("Failed to allocate memory for SVO_LeafNode child.");
            }
            node->children[i]->isLeaf = true;
            node->children[i]->depth = (depth + 1);
            init_bounding_box(parentBB, node->children[i], i);
        }
    } else {
        for (int i = 0; i < MAX_CHILDREN; ++i) {
            node->children[i] = (SVO_Node*)malloc(sizeof(SVO_Node));
            if (node->children[i] == NULL) {
                perror("Failed to allocate memory for SVO_node child #%i");
            }
            node->children[i]->internal_node = (SVO_InternalNode*)malloc(sizeof(SVO_InternalNode));
            if (node->children[i]->internal_node == NULL) {
                perror("Failed to allocate memory for SVO_InternalNode child.");
            }
            node->children[i]->isLeaf = false;
            node->children[i]->depth = (depth + 1);
            init_bounding_box(parentBB, node->children[i], i);
        }
    }
}

static void svo_count_voxels(SVO_Node* node, SVO_Node** voxels, int* voxelCount) {
    if (!node) return;

    if (node->isLeaf) {
        voxels[*voxelCount] = node;
        (*voxelCount)++;
    } else {
        for (int i = 0; i < MAX_CHILDREN; ++i) {
            svo_count_voxels(node->internal_node->children[i], voxels, voxelCount);
        }
    }
}

SVO_Node** svo_get_voxels(SVO_Node* root, int* voxelCount) {
    size_t maxVoxels = (size_t)pow(8, MAX_DEPTH);
    *voxelCount = 0;
    SVO_Node** voxels = (SVO_Node**)malloc(maxVoxels * sizeof(SVO_Node*));
    if (!voxels) {
        fprintf(stderr, "Failed to allocate memory for voxel array\n");
        return NULL;
    }

    svo_count_voxels(root, voxels, voxelCount);

    SVO_Node** packed_voxels = (SVO_Node**)realloc(voxels, *voxelCount * sizeof(SVO_Node*));
    if (!packed_voxels) {
        fprintf(stderr, "Failed to realloc packed voxels.\n");
        free(voxels);
        return NULL;
    }

    return packed_voxels;
}
