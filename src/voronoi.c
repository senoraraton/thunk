#include "voronoi.h"

void generate_seed_points(SeedPoint* points, int num_points, int width, int height) {
    srand(time(NULL));
    for (int i = 0; i < num_points; i++) {
        points[i].x = rand() % width;
        points[i].y = rand() % height;
    }
}

void compute_voronoi(GridPoint* grid, SeedPoint* points, int num_points, int width, int height) {
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            int nearest_point = -1;
            double nearest_distance = INFINITY;
            for (int i = 0; i < num_points; i++) {
                double dx = x - points[i].x;
                double dy = y - points[i].y;
                double distance = sqrt(dx * dx + dy * dy);
                if (distance < nearest_distance) {
                    nearest_distance = distance;
                    nearest_point = i;
                }
            }
            grid[y * width + x].x = x;
            grid[y * width + x].y = y;
            grid[y * width + x].region = nearest_point;
        }
    }
}
