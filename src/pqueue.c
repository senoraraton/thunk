#include <stdlib.h>
#include <stdio.h>
#include "pqueue.h"

void swap(Event* a, Event* b) {
    Event temp = *a;
    *a = *b;
    *b = temp;
}

void minHeapify(Event* array, int size, int idx) {
    int smallest = idx;
    int left = 2 * idx + 1;
    int right = 2 * idx + 2;

    if (left < size && (array[left].y < array[smallest].y || (array[left].y == array[smallest].y && array[left].x < array[smallest].x)))
        smallest = left;

    if (right < size && (array[right].y < array[smallest].y || (array[right].y == array[smallest].y && array[right].x < array[smallest].x)))
        smallest = right;

    if (smallest != idx) {
        swap(&array[idx], &array[smallest]);
        minHeapify(array, size, smallest);
    }
}

PriorityQueue* createPriorityQueue(int capacity) {
    PriorityQueue* pq = (PriorityQueue*)malloc(sizeof(PriorityQueue));
    if (!pq) {
        perror("Memory allocation failed");
        return NULL;
    }
    pq->capacity = capacity;
    pq->size = 0;
    pq->array = (Event*)malloc(capacity * sizeof(Event));
    if (!pq->array) {
        perror("Memory allocation failed");
        free(pq);
        return NULL;
    }
    return pq;
}

void destroyPriorityQueue(PriorityQueue* pq) {
    if (pq) {
        free(pq->array);
        free(pq);
    }
}

void insertEvent(PriorityQueue* pq, Event event) {
    if (pq->size == pq->capacity) {
        fprintf(stderr, "Priority queue overflow\n");
        return;
    }

    int i = pq->size;
    pq->array[i] = event;
    pq->size++;

    while (i > 0 && (pq->array[(i - 1) / 2].y > pq->array[i].y || (pq->array[(i - 1) / 2].y == pq->array[i].y && pq->array[(i - 1) / 2].x > pq->array[i].x))) {
        swap(&pq->array[i], &pq->array[(i - 1) / 2]);
        i = (i - 1) / 2;
    }
}

Event extractMin(PriorityQueue* pq) {
    if (pq->size <= 0) {
        fprintf(stderr, "Priority queue underflow\n");
        Event emptyEvent = {0};
        return emptyEvent;
    }

    Event minEvent = pq->array[0];
    pq->array[0] = pq->array[pq->size - 1];
    pq->size--;

    minHeapify(pq->array, pq->size, 0);

    return minEvent;
}

Event* findEvent(PriorityQueue* pq, Event event) {
    for (int i = 0; i < pq->size; ++i) {
        if (pq->array[i].x == event.x && pq->array[i].y == event.y && pq->array[i].type == event.type) {
            return &pq->array[i];
        }
    }
    return NULL;
}

void removeEvent(PriorityQueue* pq, Event event) {
    Event* foundEvent = findEvent(pq, event);
    if (!foundEvent) {
        fprintf(stderr, "Event not found in the priority queue\n");
        return;
    }

    int idx = foundEvent - pq->array;

    pq->array[idx] = pq->array[pq->size - 1];
    pq->size--;

    if (idx < pq->size) {
        if (idx > 0 && (pq->array[(idx - 1) / 2].y > pq->array[idx].y || (pq->array[(idx - 1) / 2].y == pq->array[idx].y && pq->array[(idx - 1) / 2].x > pq->array[idx].x))) {
            while (idx > 0 && (pq->array[(idx - 1) / 2].y > pq->array[idx].y || (pq->array[(idx - 1) / 2].y == pq->array[idx].y && pq->array[(idx - 1) / 2].x > pq->array[idx].x))) {
                swap(&pq->array[idx], &pq->array[(idx - 1) / 2]);
                idx = (idx - 1) / 2;
            }
        } else {
            minHeapify(pq->array, pq->size, idx);
        }
    }
}
