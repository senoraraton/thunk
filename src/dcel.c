#include "dcel.h"

void traverseFace(Face* face) {
    if (face == NULL || face->outerComponent == NULL) {
        printf("Face struct contains NULL impelmentation.\n");
        return;
    }

    HalfEdge* start = face->outerComponent;
    HalfEdge* current = start;

    do {
        printf("Vertex at (%.2f, %.2f)\n", current->origin->x, current->origin->y);
        current = current->next;
    } while (current != start);
}
