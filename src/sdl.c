#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include "sdl.h"

SDL_Window* sdl_begin(char* name, int width, int height, uint32_t windowFlags) {
    SDL_Window* win;
    SDL_GLContext ctx;

    if (SDL_Init(SDL_INIT_EVERYTHING) != 0){
        fprintf(stderr, "Failed to init SDL: %s\n", SDL_GetError());
        exit(1);
    }

    win = SDL_CreateWindow(name, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
           width, height, windowFlags);
    if (win== NULL) {
        fprintf(stderr, "Window could not be created.  SDL_Error: %s\n", SDL_GetError());
        exit(1);
    }

    ctx = SDL_GL_CreateContext(win);
    if(ctx == NULL){
        printf("Failed to init SDL_Gl context! SDL_Error: %s\n", SDL_GetError());
        return NULL;

    } 
    SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 4 );                                                                              
    SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 6 );                                                                              
    SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );

    glewExperimental = GL_TRUE;
    GLenum glewError = glewInit();

    if( glewError != GLEW_OK){
        printf("Error initialiing GLEW! %s\n", glewGetErrorString(glewError));
    }

    if(SDL_GL_SetSwapInterval(1) < 0){
        printf("Warning: Unable to set VSync! SDL_Error: %s\n", SDL_GetError());
    }
    printf("OpenGL Version: %s\n", glGetString(GL_VERSION));
    return win;
}

SDL_SYSWM_TYPE sdl_test_wayland(SDL_Window* win){
    SDL_SysWMinfo info;
    SDL_VERSION(&info.version);
    SDL_bool got_info = SDL_GetWindowWMInfo(win, &info);
    printf("info.subsystem %d", info.subsystem);
    if (!got_info) {
        SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION,
            "Could not retrieve window information: %s", SDL_GetError());
        return SDL_SYSWM_UNKNOWN;
    }
    else if (info.subsystem == SDL_SYSWM_WAYLAND) {
        SDL_Log("Using Wayland - SUCCESS");
    }
    else {
        SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION,
            "Not using Wayland - FAILURE");
    }
    return info.subsystem;

}

void sdl_end(SDL_Window* win) {
    SDL_DestroyWindow(win);
    SDL_Quit();
}
