#include <GL/glew.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <svo.h>
#include "gl_helper.h"

GLuint compileShaderFromSource(GLenum shaderType, const char* source) {
    GLint compileStatus;
    GLuint shader = glCreateShader(shaderType);
    glShaderSource(shader, 1, &source, NULL);
    glCompileShader(shader);
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compileStatus);
    glShaderCheckError(shader);
    return shader;
}

GLuint compileShaderFromFile(GLenum shaderType, const char* filePath) {
    FILE* file = fopen (filePath, "rb" );
    if(!file) {
        fprintf(stderr, "Failed to open shader file %s\n", filePath);
        return 0;
    }

    fseek(file, 0L , SEEK_END);
    long lSize = ftell(file);
    rewind(file);

    char* source = calloc(1, lSize+1);
    if(!source){
        fclose(file);
        fprintf(stderr, "Memory alloc for file source failed");
        exit(1);
    }

    if(1 != fread(source , lSize, 1 , file)) {
        fclose(file);
        free(source);
        fprintf(stderr, "File read failed.");
        exit(1);
    }
    fclose(file);

    GLuint shader = glCreateShader(shaderType);
    glShaderSource(shader, 1, (const GLchar**)&source, NULL);
    glCompileShader(shader);

    glShaderCheckError(shader);
    free(source);
    return shader;
}

GLuint linkProgramFromShaders(GLuint vertexShader, GLuint fragmentShader) {
    GLuint program = glCreateProgram();
    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);
    glLinkProgram(program);

    glProgramCheckError(program);
    return program;
}

GLfloat* glPackVoxels(SVO_Node** packed_voxels, int* voxelCount, bool normalize){
    if(normalize){
        normalizeVertices(packed_voxels, voxelCount);
    }
    GLfloat* vertices = calloc(3 * *voxelCount, sizeof *vertices);
    if(!vertices){                                                                                                                             
        fprintf(stderr, "Failed to allocate memory for vertex array\n");                                                                       
        return NULL;                                                                                                                             
    }                                                                                                                                          
                                                                                                                                           
    for(int i = 0; i < *voxelCount; ++i) {                                                                                                     
        vertices[3 * i + 0] = packed_voxels[i]->bounding_box.center[0];                                                                        
        vertices[3 * i + 1] = packed_voxels[i]->bounding_box.center[1];                                                                        
        vertices[3 * i + 2] = packed_voxels[i]->bounding_box.center[2];                                                                        
    }
    return vertices;
}

void glShaderCheckError(GLuint shader){
    GLint compileStatus;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compileStatus);
    if (compileStatus != GL_TRUE) {
        GLint logLength;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
        char* log = malloc(logLength);
        glGetShaderInfoLog(shader, logLength, NULL, log);
        fprintf(stderr, "Shader compilation failed: %s\n", log);
        free(log);
        glDeleteShader(shader);
        shader = 0;
    }
}

void glProgramCheckError(GLuint program){
    GLint linkStatus;

    glGetProgramiv(program, GL_LINK_STATUS, &linkStatus);
    if (linkStatus != GL_TRUE) {
        GLint logLength;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
        char* log = malloc(logLength);
        glGetProgramInfoLog(program, logLength, NULL, log);
        fprintf(stderr, "Program linking failed: %s\n", log);
        free(log);
        glDeleteProgram(program);
    }
}

//Fix this to be relative normalied coords.
void normalizeVertices(SVO_Node** packed_voxels, int *voxelCount) {
    for (int i = 0; i < *voxelCount; ++i) {
        packed_voxels[i]->bounding_box.center[0] = (packed_voxels[i]->bounding_box.center[0] / 200.0) - 1.0f;
        packed_voxels[i]->bounding_box.center[1] = (packed_voxels[i]->bounding_box.center[1] / 200.0) - 1.0f;
        packed_voxels[i]->bounding_box.center[2] = (packed_voxels[i]->bounding_box.center[2] / 200.0) - 1.0f;
    }
}
