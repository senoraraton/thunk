#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include "sdl.h"
#include "svo.h"
#include "gl_helper.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600


#define CHECK_GL_ERROR() \
    { \
        GLenum err; \
        while ((err = glGetError()) != GL_NO_ERROR) { \
            printf("OpenGL error: %d, at line %d\n", err, __LINE__); \
        } \
    }

void render(GLuint program, GLfloat* vertexData, int* voxelCount){

    glEnable(GL_PROGRAM_POINT_SIZE);
    glViewport(0, 0, WIN_WIDTH, WIN_HEIGHT);
    glClearColor(0.f, 0.f, 0.f, 0.f);
    glClear(GL_COLOR_BUFFER_BIT);

    static GLuint vao = 0;
    static GLuint vbo = 0;

    if(vao == 0){
        glGenVertexArrays(1, &vao);
        glGenBuffers(1, &vbo);
    }

    glBindVertexArray(vao);
    CHECK_GL_ERROR();
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    CHECK_GL_ERROR();
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 3 * (*voxelCount), vertexData, GL_STATIC_DRAW);
    CHECK_GL_ERROR();
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_TRUE, 3 * sizeof(GLfloat), (void*)0);
    CHECK_GL_ERROR();
    glEnableVertexAttribArray(0);
    CHECK_GL_ERROR();
    glUseProgram(program);
    CHECK_GL_ERROR();
    glDrawArrays(GL_POINTS, 0, *voxelCount);
    CHECK_GL_ERROR();

    GLfloat* data = (GLfloat*)malloc(sizeof(GLfloat) * 3 * (*voxelCount));
    glGetBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(GLfloat) * 3 * (*voxelCount), data);
    free(data); // Free allocated memory


    //Cleanup
    glBindVertexArray(0);
    glUseProgram(0);
}

int main(void) {

    bool running = true;
    bool fullscreen = false;

    SVO_Node* root = svo_init_tree(400);    

    unsigned int c = svo_count_nodes(root);
    printf("Number of nodes in tree: %u\n", c);

    SDL_Window* win;
    SDL_GLContext ctx;
    uint32_t windowFlags = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN;

    if (SDL_Init(SDL_INIT_EVERYTHING) != 0){
        fprintf(stderr, "Failed to init SDL: %s\n", SDL_GetError());
        exit(1);
    }

    SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 4 );                                                                              
    SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 6 );                                                                              
    SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    win = SDL_CreateWindow("OpenGL Test.", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
           WIN_WIDTH, WIN_HEIGHT, windowFlags);
    if (win == NULL) {
        fprintf(stderr, "Window could not be created.  SDL_Error: %s\n", SDL_GetError());
        exit(1);
    }

    ctx = SDL_GL_CreateContext(win);
    if(ctx == NULL){
        printf("Failed to init SDL_Gl context! SDL_Error: %s\n", SDL_GetError());
        return -1;

    } 

    glewExperimental = GL_TRUE;
    GLenum glewError = glewInit();

    if( glewError != GLEW_OK){
        printf("Error initialiing GLEW! %s\n", glewGetErrorString(glewError));
    }

    if(SDL_GL_SetSwapInterval(1) < 0){
        printf("Warning: Unable to set VSync! SDL_Error: %s\n", SDL_GetError());
    }

    GLuint vertexShader = compileShaderFromFile(GL_VERTEX_SHADER, "./shaders/voxel_pixel.vert");
    GLuint fragmentShader = compileShaderFromFile(GL_FRAGMENT_SHADER, "./shaders/voxel_pixel.frag");

    if (vertexShader == 0 || fragmentShader == 0) {
        fprintf(stderr, "Failed to compile shaders\n");
        return -1;
    }

    GLuint shaderProgram = linkProgramFromShaders(vertexShader, fragmentShader);
    if (shaderProgram == 0) {
        fprintf(stderr, "Failed to link shader program\n");
        return -1;
    }

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    int* voxelCount= malloc(sizeof *voxelCount);
    if (!voxelCount) {
        fprintf(stderr, "Failed to allocate memory for voxelCount\n");
        return -1;
    }
    SVO_Node** packed_voxels = svo_get_voxels(root, voxelCount);
    GLfloat* vertices = glPackVoxels(packed_voxels, voxelCount, true);
    while(running){
        SDL_Event Event;
        while (SDL_PollEvent(&Event)){
            if (Event.type == SDL_KEYDOWN){
                switch (Event.key.keysym.sym)
                {
                    case SDLK_ESCAPE:
                        running = 0;
                        break;
                    case 'f':
                        fullscreen = !fullscreen;
                        if (fullscreen){
                            SDL_SetWindowFullscreen(win, windowFlags | SDL_WINDOW_FULLSCREEN_DESKTOP);
                        } else {
                            SDL_SetWindowFullscreen(win, windowFlags);
                        }
                        break;
                    default:
                        break;
                    }
            } else if (Event.type == SDL_QUIT){
                running = 0;
            }
        }


        render(shaderProgram, vertices, voxelCount);

        /*render(shaderProgram);*/
        /*gl_draw_hello_world_triangle();*/
        SDL_Delay(100);

        SDL_GL_SwapWindow(win);
    }
    free(vertices);
    svo_destroy_tree(root);
    return 0;
}
