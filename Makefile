CC = cc
CFLAGS = -std=c11 -Iinclude -Isrc -Itest -g -Wall -Wextra -Wpedantic 
LDFLAGS = -lSDL2 -lglfw -lGLEW -lGL -lGLU
BUILD_DIR = build
OBJ_BUILD_DIR = $(BUILD_DIR)/OBJ
OBJ_TEST_DIR = $(BUILD_DIR)/Test_OBJ

# Source files
SRC_FILES = $(wildcard src/*.c)
TEST_FILES = $(wildcard test/*.c)
OBJ_BUILD_FILES = $(patsubst src/%.c, $(OBJ_BUILD_DIR)/%.o, $(SRC_FILES))
OBJ_TEST_FILES = $(patsubst test/%.c, $(OBJ_TEST_DIR)/%.o, $(TEST_FILES))

# Targets
$(BUILD_DIR)/main: $(OBJ_BUILD_FILES)
	@mkdir -p $(@D)
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

$(BUILD_DIR)/test: $(OBJ_TEST_FILES) 
	@mkdir -p $(@D)
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

$(OBJ_BUILD_DIR)/%.o: src/%.c
	@mkdir -p $(@D)
	$(CC) $(CFLAGS) -MMD -c $< -o $@

$(OBJ_TEST_DIR)/%.o: test/%.c
	@mkdir -p $(@D)
	$(CC) $(CFLAGS) -MMD -c $< -o $@

all: main test

main: $(BUILD_DIR)/main

test: $(BUILD_DIR)/test

clean:
	rm -rf $(BUILD_DIR)

rmain: $(BUILD_DIR)/main
	@echo "Running main project..."
	@./$(BUILD_DIR)/main

rtest: $(BUILD_DIR)/test
	@echo "Running tests..."
	@./$(BUILD_DIR)/test

out: $(BUILD_DIR)/main
	@./$(BUILD_DIR)/main > out.txt 2>&1
	@nvim out.txt

.PHONY: all clean rmain rtest main test

-include $(OBJ_BUILD_FILES:.o=.d) $(OBJ_TEST_FILES:.o=.d)
